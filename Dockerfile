# This is the image use to run the gitlab-ci
# use this image registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
FROM registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

COPY . .

# Download and install required tools.
# pulumi
RUN curl -fsSL https://get.pulumi.com/ | bash
ENV PATH="$PATH:$HOME/.pulumi/bin"

# update the GitLab Runner's packages
RUN apt-get update -y
RUN apt-get install sudo -y

# Install kubectl
RUN apt-get install -y ca-certificates curl
RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update -y
RUN apt-get install -y kubectl

# nodejs
RUN curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
RUN apt-get install -y nodejs

# install python dependencies
RUN apt-get install python3-venv -y

ENV VIRTUAL_ENV=/venv
RUN python3 -m venv $VIRTUAL_ENV
RUN chmod +x venv/bin/*
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip3 install wheel
RUN pip3 install -r requirements.txt
